import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractURL {
    private URL url;

    public ExtractURL(URL url) throws MalformedURLException {
            this.url = new URL(url.toString());
    }

    public URL getUrl() throws MalformedURLException {
        return new URL(url.toString());
    }

    public void setUrl(URL url) throws MalformedURLException {
        this.url = new URL(url.toString());
    }

    public List extract(Pattern pattern, int limit) throws IOException {
        List list = new ArrayList();
        int startLimit = 0;
        try  (BufferedReader buffer = new BufferedReader(new InputStreamReader(url.openStream())) ) {
            String pageLine;
            while ((pageLine = buffer.readLine()) != null) {
                Matcher matcher = pattern.matcher(pageLine);
                while (matcher.find() && startLimit <= limit){
                    list.add(matcher.group());
                    startLimit++;
                }
            }
        } catch (IOException e) {
            throw new IOException();

        }

        return list;
    }

    public List extract(Pattern pattern) throws IOException {
        List list = new ArrayList();
        try  (BufferedReader buffer = new BufferedReader(new InputStreamReader(url.openStream())) ) {
            String pageLine;

            while ((pageLine = buffer.readLine()) != null) {
                Matcher matcher = pattern.matcher(pageLine);
                while (matcher.find()){
                    list.add(matcher.group());
                }
            }
        } catch (IOException e) {
            throw new IOException();

        }
        return list;
    }
}
