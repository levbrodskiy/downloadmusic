import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String url;

        if ((url = scanner.nextLine()).isEmpty()){
            url = "https://muzika.vip/";
        }

        Pattern patternMusic = Pattern.compile(Constans.PATTERN_MUSIC);
        List list = new ArrayList();
        try {
            ExtractURL extractURL = new ExtractURL(new URL(url));
            list = extractURL.extract(patternMusic);

            for (int i = 0; i < 1; i++) {
                System.out.println(list.get(i));
                Downloader downloader = new Downloader(new URL(list.get(i).toString()), "src\\music.mp3");
                downloader.downloadWithNIO();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}