import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;


public class Downloader {
    private URL url;
    private String outputWay;


    public Downloader(URL url, String outputWay) {
        this.url = url;
        this.outputWay = outputWay;
    }

    public URL getUrl(){
        return url;
    }

    public String getOutputWay(){
        return outputWay;
    }

    public void setUrl(URL url){
        this.url = url;
    }

    public void setOutputWay(String outputWay){
        this.outputWay = outputWay;
    }
    /**
     * Использования чтения из файла с ограничением потока в 2^63 байта
     *
     * @throws Exception
     */
    public void downloadWithNIO() throws IOException {

        FileOutputStream stream;
        try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream())) {
            stream = new FileOutputStream(outputWay);
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        }catch (IOException e) {
            throw new IOException();
        }
    }

}
